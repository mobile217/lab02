import 'dart:io';

void main(List<String> arguments) {
  print("input lottery :");
  int lottery = int.parse( stdin.readLineSync( )! );
  check(lottery);
}

void check(int lottery){
  int st1 = 836777;
  int page31 = 950;
  int page32 = 273;
  int last31 = 131;
  int last32 = 362;
  int last2 = 63;
  int count = 0;
  if(lottery == st1 ){
    print("won 1st prize 6,000,000 baht");
    count++;
  }
  if((lottery/1000).toInt() == page31 || (lottery/1000).toInt() == page32){
    print("won 3 page numbers 4,000 baht");
    count++;
  }
  if((lottery-((lottery/1000).toInt()*1000)) == last31 || (lottery-((lottery/1000).toInt()*1000)) == last32){
    print("won 3 last numbers 4,000 baht");
    count++;
  }
  if((lottery-((lottery/100).toInt()*100)) == last2){
    print("won 2 last numbers 2,000 baht");
    count++;
  }
  if(count == 0){
    print("Sorry you didn't get the award.");
  }
}